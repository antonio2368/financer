package hr.financer.repositories;

import hr.financer.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {
    boolean existsByName(String name);
}
