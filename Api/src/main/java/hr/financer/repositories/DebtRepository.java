package hr.financer.repositories;

import hr.financer.models.entities.Debt;
import hr.financer.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DebtRepository extends JpaRepository<Debt, Integer> {
    Debt findByUserAndDebtToUser(User user, User debtToUser);
    List<Debt> findAllByDebtToUser(User debtToUser);
    List<Debt> findAllByUser(User user);
}
