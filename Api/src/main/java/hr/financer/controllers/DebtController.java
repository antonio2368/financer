package hr.financer.controllers;

import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.debt.BorrowDto;
import hr.financer.models.dto.debt.DebtChangeDto;
import hr.financer.models.dto.debt.UserDebtDto;
import hr.financer.services.interfaces.IDebtService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for controlling debts
 */
@CrossOrigin
@RequestMapping("/api/debt")
@RestController
public class DebtController {
    private Logger logger = LoggerFactory.getLogger(DebtController.class);

    @Autowired
    private IDebtService debtService;

    /**
     * Add new expense
     * @param dto Model containing info about the expense
     */
    @PostMapping("/expense")
    public ResponseEntity addExpense(@RequestBody DebtChangeDto dto) {
        try {
            debtService.addExpense(dto);
            return ResponseEntity.ok().build();
        } catch (ValidationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Add new earning
     * @param dto Model containing info about earning
     */
    @PostMapping("/earning")
    public ResponseEntity addEarning(@RequestBody DebtChangeDto dto) {
        try {
            debtService.addEarning(dto);
            return ResponseEntity.ok().build();
        } catch (ValidationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Method for borrowing money between users
     * @param dto Model containing info for borrowing
     */
    @PostMapping("/borrow")
    public ResponseEntity borrow(@RequestBody BorrowDto dto) {
        try {
            debtService.borrow(dto);
            return ResponseEntity.ok().build();
        } catch (ValidationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Get all debts for a user
     * @param id Id of the user for which all debts are needed
     * @return All debts for the specified user
     */
    @GetMapping("/{id}")
    public ResponseEntity getDebtForUser(@PathVariable int id) {
        try {
            List<UserDebtDto> userDebt = debtService.getDebtForUser(id);
            return ResponseEntity.ok(userDebt);
        } catch (NotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }



}
