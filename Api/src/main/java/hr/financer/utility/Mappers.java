package hr.financer.utility;

import hr.financer.models.dto.debt.UserDebtDto;
import hr.financer.models.dto.user.UserDto;
import hr.financer.models.dto.user.UserPostDto;
import hr.financer.models.entities.Debt;
import hr.financer.models.entities.User;

public class Mappers {
    public static UserDto MapToUserDto(User user) {
        UserDto dto = new UserDto();
        dto.setName(user.getName());
        dto.setUserId(user.getUserId());
        return dto;
    }

    public static User MapToUser(UserPostDto dto) {
        User user = new User();
        user.setName(dto.getName());

        return user;
    }

    public static UserDebtDto MapToUserDebtDto(Debt debt) {
        UserDebtDto dto = new UserDebtDto();

        UserDto debtToUser = MapToUserDto(debt.getDebtToUser());

        dto.setAmount(debt.getAmount());
        dto.setDebtToUser(debtToUser);

        return dto;
    }
}
