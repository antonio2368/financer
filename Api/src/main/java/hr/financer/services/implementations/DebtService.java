package hr.financer.services.implementations;

import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.debt.BorrowDto;
import hr.financer.models.dto.debt.DebtChangeDto;
import hr.financer.models.dto.debt.UserDebtDto;
import hr.financer.models.entities.Debt;
import hr.financer.models.entities.User;
import hr.financer.repositories.DebtRepository;
import hr.financer.repositories.UserRepository;
import hr.financer.services.interfaces.IDebtService;
import hr.financer.utility.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DebtService implements IDebtService {
    @Autowired
    private DebtRepository debtRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void addExpense(DebtChangeDto dto) throws ValidationException {
        if (dto == null) {
            throw new IllegalArgumentException("Model is null");
        }

        if (!userRepository.existsById(dto.getUserId())) {
            throw new ValidationException("Paying user has unknown id");
        }

        if (dto.getMemberIds() == null || dto.getMemberIds().size() == 0) {
            throw new ValidationException("No members picked");
        }

        for (int userId : dto.getMemberIds()) {
            if (!userRepository.existsById(userId)) {
                throw new ValidationException("User with id '" + userId + "' does not exist");
            }
        }

        if (dto.getAmount() <= 0) {
            throw new ValidationException("Expense cannot be negative or 0");
        }

        changeDebts(dto);
    }

    @Override
    public void addEarning(DebtChangeDto dto) throws ValidationException {
        if (dto == null) {
            throw new IllegalArgumentException("Model is null");
        }

        if (!userRepository.existsById(dto.getUserId())) {
            throw new ValidationException("Collecting user has unknown id");
        }

        if (dto.getMemberIds() == null || dto.getMemberIds().size() == 0) {
            throw new ValidationException("No members picked");
        }

        for (int userId : dto.getMemberIds()) {
            if (!userRepository.existsById(userId)) {
                throw new ValidationException("User with id '" + userId + "' does not exist");
            }
        }

        if (dto.getAmount() <= 0) {
            throw new ValidationException("Earning cannot be negative or 0");
        }

        // earnings reduce debts of each member to the user that collected the money
        dto.setAmount(-dto.getAmount());
        changeDebts(dto);
    }

    @Override
    public void borrow(BorrowDto dto) throws ValidationException {
        if (dto == null) {
            throw new IllegalArgumentException("Model is null");
        }

        if (!userRepository.existsById(dto.getLenderId())) {
            throw new ValidationException("Lender has unknown id");
        }

        if (!userRepository.existsById(dto.getBorrowerId())) {
            throw new ValidationException("Borrower has unknown id");
        }

        if (dto.getAmount() <= 0) {
            throw new ValidationException("Cannot borrow 0 or negative amount of money");
        }

        if (dto.getLenderId() == dto.getBorrowerId()) {
            throw new ValidationException("Cannot borrow to himself");
        }

        changeDebt(dto.getBorrowerId(), dto.getLenderId(), dto.getAmount());
    }

    @Override
    public List<UserDebtDto> getDebtForUser(int userId) throws NotFoundException {
        User user = userRepository.findById(userId).orElse(null);

        if (user == null) {
            throw new NotFoundException();
        }

        List<Debt> debt = Stream.concat(
            debtRepository.findAllByUser(user).stream(),
            debtRepository.findAllByDebtToUser(user)
                            .stream()
                            .map(d -> {
                                User temp = d.getDebtToUser();
                                d.setDebtToUser(d.getUser());
                                d.setUser(temp);
                                d.setAmount(-d.getAmount());
                                return d;
                            })
        ).collect(Collectors.toList());


        return debt.stream()
                .map(Mappers::MapToUserDebtDto)
                .collect(Collectors.toList());
    }

    private void changeDebts(DebtChangeDto dto) throws ValidationException {
        double amountPerPerson = dto.getAmount() / dto.getMemberIds().size();

        for (int memberId : dto.getMemberIds()) {
            if (memberId != dto.getUserId()) {
                changeDebt(memberId, dto.getUserId(), amountPerPerson);
            }
        }
    }

    private void changeDebt(int userId, int debtToUserId, double change) {
        // not necessary to save both directions of debt
        // e.g. if A has smaller id than B, only As debt to B will be saved
        if (debtToUserId < userId) {
            change = -change;
            int temp = debtToUserId;
            debtToUserId = userId;
            userId = temp;
        }

        User user = userRepository.findById(userId).orElse(null);
        User debtToUser = userRepository.findById(debtToUserId).orElse(null);
        Debt debt = debtRepository.findByUserAndDebtToUser(user, debtToUser);

        if (debt == null) {
            debt = new Debt();

            debt.setUser(user);
            debt.setDebtToUser(debtToUser);
            debt.setAmount(change);

            debtRepository.saveAndFlush(debt);
            return;
        }

        debt.setAmount(debt.getAmount() + change);
        debtRepository.saveAndFlush(debt);
    }
}
