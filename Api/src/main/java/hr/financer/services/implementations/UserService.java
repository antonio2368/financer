package hr.financer.services.implementations;

import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.debt.UserDebtDto;
import hr.financer.models.dto.user.UserDto;
import hr.financer.models.dto.user.UserPostDto;
import hr.financer.models.entities.User;
import hr.financer.repositories.UserRepository;
import hr.financer.services.interfaces.IDebtService;
import hr.financer.services.interfaces.IUserService;
import hr.financer.utility.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements IUserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IDebtService debtService;

    @Override
    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream().map(u -> {
            UserDto dto = Mappers.MapToUserDto(u);

            try {
                dto.setState(
                        debtService.getDebtForUser(u.getUserId())
                                .stream()
                                .map(UserDebtDto::getAmount)
                                .reduce(0.0, (curr, acc) -> curr + acc)
                );
            } catch (Exception e) {
                e.printStackTrace();
            }
            return dto;
        }).collect(Collectors.toList());
    }

    @Override
    public UserDto addUser(UserPostDto model) throws ValidationException {
        if (model == null) {
            throw new IllegalArgumentException("Model is null");
        }

        if (model.getName() == null || model.getName().isEmpty()) {
            throw new ValidationException("Name is required");
        }

        if (userRepository.existsByName(model.getName())) {
            throw new ValidationException("Name already exists");
        }

        User user = Mappers.MapToUser(model);
        userRepository.saveAndFlush(user);

        return Mappers.MapToUserDto(user);
    }

    @Override
    public void deleteUser(int userId) throws NotFoundException {
        User user = userRepository.findById(userId).orElse(null);

        if (user == null) {
            throw new NotFoundException();
        }

        userRepository.delete(user);
    }
}
