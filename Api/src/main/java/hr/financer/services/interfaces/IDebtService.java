package hr.financer.services.interfaces;

import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.debt.BorrowDto;
import hr.financer.models.dto.debt.DebtChangeDto;
import hr.financer.models.dto.debt.UserDebtDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IDebtService {
    void addExpense(DebtChangeDto dto) throws ValidationException;
    void addEarning(DebtChangeDto dto) throws ValidationException;
    void borrow(BorrowDto dto) throws ValidationException;
    List<UserDebtDto> getDebtForUser(int userId) throws NotFoundException;
}
