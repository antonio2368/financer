package hr.financer.services.interfaces;

import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.user.UserDto;
import hr.financer.models.dto.user.UserPostDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IUserService {
    List<UserDto> getAllUsers();
    UserDto addUser(UserPostDto model) throws ValidationException;
    void deleteUser(int userId) throws NotFoundException;
}