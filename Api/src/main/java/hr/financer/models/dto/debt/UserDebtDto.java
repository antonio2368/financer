package hr.financer.models.dto.debt;

import hr.financer.models.dto.user.UserDto;

public class UserDebtDto {
    private UserDto debtToUser;
    private double amount;

    public UserDto getDebtToUser() {
        return debtToUser;
    }

    public void setDebtToUser(UserDto debtToUser) {
        this.debtToUser = debtToUser;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


}
