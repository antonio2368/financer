package hr.financer.services;

import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.debt.UserDebtDto;
import hr.financer.models.dto.user.UserDto;
import hr.financer.models.dto.user.UserPostDto;
import hr.financer.models.entities.User;
import hr.financer.repositories.UserRepository;
import hr.financer.services.interfaces.IDebtService;
import hr.financer.services.interfaces.IUserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {
    @Autowired
    private IUserService userService;

    @MockBean
    private IDebtService debtService;

    @MockBean
    private UserRepository userRepository;

    @Test
    public void whenGetAllUsers_thenReturnAllUsers() throws Exception {
        User testUser = new User();
        testUser.setName("Marko");

        UserDto debtToUser = new UserDto();
        debtToUser.setName("Josip");

        UserDebtDto debt = new UserDebtDto();
        debt.setDebtToUser(debtToUser);
        debt.setAmount(32);

        List<User> allUsers = Arrays.asList(testUser);
        List<UserDebtDto> debts = Arrays.asList(debt);

        given(userRepository.findAll()).willReturn(allUsers);
        given(debtService.getDebtForUser(any(Integer.class))).willReturn(debts);

        UserDto expectedUser = new UserDto();
        expectedUser.setName("Marko");
        expectedUser.setState(32);

        List<UserDto> result = userService.getAllUsers();
        List<UserDto> expected = Arrays.asList(expectedUser);

        Assert.assertEquals(result, expected);
    }

    @Test
    public void whenValidAdduser_thenReturnAddedUser() throws Exception {
        UserPostDto newUser = new UserPostDto();
        newUser.setName("Marko");

        given(userRepository.existsByName("Marko")).willReturn(false);
        given(userRepository.saveAndFlush(any(User.class))).willReturn(null);

        UserDto result = userService.addUser(newUser);
        Assert.assertEquals(result.getName(), newUser.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenAddUserModelIsNull_thenThrowException() throws Exception {
        userService.addUser(null);
    }

    @Test(expected = ValidationException.class)
    public void whenAddUserNameIsNull_thenThrowException() throws Exception {
        UserPostDto newUser = new UserPostDto();
        userService.addUser(newUser);
    }

    @Test(expected = ValidationException.class)
    public void whenAddUserNameIsEmpty_thenThrowException() throws Exception {
        UserPostDto newUser = new UserPostDto();
        newUser.setName("");

        userService.addUser(newUser);
    }

    @Test(expected = ValidationException.class)
    public void whenAddUserNameExists_thenThrowException() throws Exception {
        UserPostDto newUser = new UserPostDto();
        newUser.setName("Existing name");

        given(userRepository.existsByName("Existing name")).willReturn(true);

        userService.addUser(newUser);
    }

    @Test(expected = NotFoundException.class)
    public void whenDeleteUserNotFound_thenThrowException() throws Exception {
        given(userRepository.findById(0)).willReturn(Optional.empty());
        userService.deleteUser(0);
    }

    @Test
    public void whenDeleteUserSuccesful_returnNothing() throws Exception {
        given(userRepository.findById(0)).willReturn(Optional.of(new User()));
        doNothing().when(userRepository).delete(any(User.class));
    }

}
