package hr.financer.services;

import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.debt.BorrowDto;
import hr.financer.models.dto.debt.DebtChangeDto;
import hr.financer.models.dto.debt.UserDebtDto;
import hr.financer.models.entities.Debt;
import hr.financer.models.entities.User;
import hr.financer.repositories.DebtRepository;
import hr.financer.repositories.UserRepository;
import hr.financer.services.interfaces.IDebtService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DebtServiceTest {
    @Autowired
    private IDebtService debtService;

    @MockBean
    private DebtRepository debtRepository;

    @MockBean
    private UserRepository userRepository;

    @Test(expected = IllegalArgumentException.class)
    public void whenAddExpenseModelIsNull_thenThrowException() throws Exception {
        debtService.addExpense(null);
    }

    @Test(expected = ValidationException.class)
    public void whenPayingUserIdNotFound_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        given(userRepository.existsById(1)).willReturn(false);
        debtService.addExpense(debtChange);
    }

    @Test(expected = ValidationException.class)
    public void whenPayingForListNull_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        given(userRepository.existsById(1)).willReturn(true);
        debtService.addExpense(debtChange);
    }

    @Test(expected = ValidationException.class)
    public void whenPayingForListIsEmpty_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        debtChange.setMemberIds(List.of());
        given(userRepository.existsById(1)).willReturn(true);
        debtService.addExpense(debtChange);
    }

    @Test(expected = ValidationException.class)
    public void whenPayingForUserIdNotFound_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        debtChange.setMemberIds(List.of(2));

        given(userRepository.existsById(1)).willReturn(true);
        given(userRepository.existsById(2)).willReturn(false);

        debtService.addExpense(debtChange);
    }

    @Test(expected = ValidationException.class)
    public void whenExpenseNegativeOrZero_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        debtChange.setMemberIds(List.of(2));
        debtChange.setAmount(-12);

        given(userRepository.existsById(1)).willReturn(true);
        given(userRepository.existsById(2)).willReturn(true);

        debtService.addExpense(debtChange);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenAddEarningModelIsNull_thenThrowException() throws Exception {
        debtService.addEarning(null);
    }

    @Test(expected = ValidationException.class)
    public void whenCollectingUserIdNotFound_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        given(userRepository.existsById(1)).willReturn(false);
        debtService.addEarning(debtChange);
    }

    @Test(expected = ValidationException.class)
    public void whenCollectingForListNull_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        given(userRepository.existsById(1)).willReturn(true);
        debtService.addEarning(debtChange);
    }

    @Test(expected = ValidationException.class)
    public void whenCollectingForListIsEmpty_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        debtChange.setMemberIds(List.of());
        given(userRepository.existsById(1)).willReturn(true);
        debtService.addEarning(debtChange);
    }

    @Test(expected = ValidationException.class)
    public void whenCollectingForUserIdNotFound_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        debtChange.setMemberIds(List.of(2));

        given(userRepository.existsById(1)).willReturn(true);
        given(userRepository.existsById(2)).willReturn(false);

        debtService.addEarning(debtChange);
    }

    @Test(expected = ValidationException.class)
    public void whenEarningNegativeOrZero_thenThrowException() throws Exception {
        DebtChangeDto debtChange = new DebtChangeDto();
        debtChange.setUserId(1);
        debtChange.setMemberIds(List.of(2));
        debtChange.setAmount(-12);

        given(userRepository.existsById(1)).willReturn(true);
        given(userRepository.existsById(2)).willReturn(true);

        debtService.addEarning(debtChange);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenBorrowModelIsNull_thenThrowException() throws Exception {
        debtService.borrow(null);
    }

    @Test(expected = ValidationException.class)
    public void whenLenderIdNotFound_thenThrowException() throws Exception {
        BorrowDto borrow = new BorrowDto();
        borrow.setLenderId(1);
        given(userRepository.existsById(1)).willReturn(false);
        debtService.borrow(borrow);
    }

    @Test(expected = ValidationException.class)
    public void whenBorrowerIdNotFound_thenThrowException() throws Exception {
        BorrowDto borrow = new BorrowDto();
        borrow.setLenderId(1);
        borrow.setBorrowerId(2);
        given(userRepository.existsById(1)).willReturn(true);
        given(userRepository.existsById(2)).willReturn(false);
        debtService.borrow(borrow);
    }

    @Test(expected = ValidationException.class)
    public void whenBorrowingNegativeOrZero_thenThrowException() throws Exception {
        BorrowDto borrow = new BorrowDto();
        borrow.setLenderId(1);
        borrow.setBorrowerId(2);
        borrow.setAmount(-12);
        given(userRepository.existsById(1)).willReturn(true);
        given(userRepository.existsById(2)).willReturn(true);
        debtService.borrow(borrow);
    }

    @Test(expected = ValidationException.class)
    public void whenBorrowerAndLenderAreSame_thenThrowException() throws Exception {
        BorrowDto borrow = new BorrowDto();
        borrow.setLenderId(1);
        borrow.setBorrowerId(1);
        borrow.setAmount(12);
        given(userRepository.existsById(1)).willReturn(true);
        debtService.borrow(borrow);
    }

    @Test(expected = NotFoundException.class)
    public void whenGetDebtForUserNotFound_thenThrowException() throws Exception {
        given(userRepository.findById(0)).willReturn(Optional.empty());
        debtService.getDebtForUser(0);
    }

    @Test
    public void whenGetDebtForUser_thenReturnListOfDebts() throws Exception {
        User user = new User();
        user.setName("Marko");

        User debtToUser = new User();
        debtToUser.setName("Josip");

        Debt debt = new Debt();
        debt.setUser(user);
        debt.setDebtToUser(debtToUser);
        debt.setAmount(32);

        given(userRepository.findById(1)).willReturn(Optional.of(user));
        given(debtRepository.findAllByUser(any(User.class))).willReturn(List.of(debt));
        given(debtRepository.findAllByDebtToUser(any(User.class))).willReturn(List.of());

        List<UserDebtDto> debts = debtService.getDebtForUser(1);
        Assert.assertEquals(32, debts.get(0).getAmount(), 1e-6);
        Assert.assertEquals("Josip", debts.get(0).getDebtToUser().getName());
    }
}
