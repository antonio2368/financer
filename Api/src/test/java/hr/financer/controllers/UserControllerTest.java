package hr.financer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.user.UserDto;
import hr.financer.models.dto.user.UserPostDto;
import hr.financer.services.implementations.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private UserService service;

    @Autowired
    ObjectMapper mapper;

    @Test
    public void whenGetAllUsers_thenReturnJsonArray() throws Exception {
        UserDto user = new UserDto();
        user.setName("Janko");

        List<UserDto> allUsers = Arrays.asList(user);

        given(service.getAllUsers()).willReturn(allUsers);

        mvc.perform(get("/api/user")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(user.getName())));
    }

    @Test
    public void whenGetAllUsers_andNoUsers_thenReturnEmptyJsonArray() throws Exception {
        List<UserDto> allUsers = Arrays.asList();

        given(service.getAllUsers()).willReturn(allUsers);

        mvc.perform(get("/api/user")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void whenAddUser_thenReturnAddedUser() throws Exception {
        UserPostDto userToAdd = new UserPostDto();
        userToAdd.setName("Matija");

        UserDto returnedUser = new UserDto();
        returnedUser.setName("Matija");

        given(service.addUser(userToAdd)).willReturn(returnedUser);

        mvc.perform(post("/api/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(userToAdd)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.name", is(returnedUser.getName())));
    }

    @Test
    public void whenAddUserFailValidation_thenReturnBadRequest() throws Exception {
        given(service.addUser(any(UserPostDto.class))).willThrow(ValidationException.class);

        mvc.perform(post("/api/user")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(new UserPostDto())))
                    .andExpect(status().isBadRequest());
    }

    @Test
    public void whenDeleteUser_thenReturnOk() throws Exception {
        doNothing().when(service).deleteUser(1);
        mvc.perform(delete("/api/user/1"))
                .andExpect(status().isOk());

    }

    @Test
    public void whenDeleteUserNotFound_theReturnNotFound() throws Exception {
        doThrow(NotFoundException.class).when(service).deleteUser(any(Integer.class));

        mvc.perform(delete("/api/user/1"))
                .andExpect(status().isNotFound());
    }

}
