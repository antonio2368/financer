package hr.financer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.financer.exception.NotFoundException;
import hr.financer.exception.ValidationException;
import hr.financer.models.dto.debt.BorrowDto;
import hr.financer.models.dto.debt.DebtChangeDto;
import hr.financer.models.dto.debt.UserDebtDto;
import hr.financer.models.dto.user.UserDto;
import hr.financer.services.implementations.DebtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(DebtController.class)
public class DebtControllerTest {
    @Autowired
    MockMvc mvc;

    @MockBean
    private DebtService service;

    @Autowired
    ObjectMapper mapper;

    @Test
    public void whenAddExpense_thenReturnOk() throws Exception {
        doNothing().when(service).addExpense(any(DebtChangeDto.class));

        mvc.perform(post("/api/debt/expense")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(new DebtChangeDto())))
                    .andExpect(status().isOk());
    }

    @Test
    public void whenAddExpenseFailsValidation_thenReturnBadRequest() throws Exception {
        doThrow(ValidationException.class).when(service).addExpense(any(DebtChangeDto.class));

        mvc.perform(post("/api/debt/expense")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest());
    }

    @Test
    public void whenAddEarning_thenReturnOk() throws Exception {
        doNothing().when(service).addEarning(any(DebtChangeDto.class));

        mvc.perform(post("/api/debt/earning")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(new DebtChangeDto())))
                .andExpect(status().isOk());
    }

    @Test
    public void whenAddEarningFailsValidation_thenReturnBadRequest() throws Exception {
        doThrow(ValidationException.class).when(service).addEarning(any(DebtChangeDto.class));

        mvc.perform(post("/api/debt/earning")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void whenBorrow_thenReturnOk() throws Exception {
        doNothing().when(service).borrow(any(BorrowDto.class));

        mvc.perform(post("/api/debt/borrow")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(new DebtChangeDto())))
                .andExpect(status().isOk());
    }

    @Test
    public void whenBorrowFailsValidation_thenReturnBadRequest() throws Exception {
        doThrow(ValidationException.class).when(service).borrow(any(BorrowDto.class));

        mvc.perform(post("/api/debt/borrow")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void whenGetDebtForUser_thenReturnJsonArray() throws Exception {
        UserDto debtToUser = new UserDto();
        debtToUser.setName("Josip");

        UserDebtDto userDebtDto = new UserDebtDto();
        userDebtDto.setDebtToUser(debtToUser);
        userDebtDto.setAmount(20);

        List<UserDebtDto> debts = Arrays.asList(userDebtDto);

        given(service.getDebtForUser(1)).willReturn(debts);

        mvc.perform(get("/api/debt/1")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(1)))
                    .andExpect(jsonPath("$[0].debtToUser.name", is(debtToUser.getName())))
                    .andExpect(jsonPath("$[0].amount", is(20.0)));
    }

    @Test
    public void whenGetDebtForUserNotFound_thenReturnNotFound() throws Exception {
        given(service.getDebtForUser(any(Integer.class))).willThrow(NotFoundException.class);

        mvc.perform(get("/api/debt/1")
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNotFound());
    }

}
