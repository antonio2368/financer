import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'financer';
  users = [];
  error = '';

  userForm: FormGroup;
  currentModal;

  pickedDebts = null;

  borrowForm: FormGroup;
  borrowError = '';

  expenseForm: FormGroup;
  expenseError = '';

  earningForm: FormGroup;
  earningError = '';

  constructor(private appService: AppService,
    private modalService: NgbModal,
    private fb: FormBuilder) {
  }

  ngOnInit() {
    this.getUsers();
    this.userForm = this.fb.group({
      name: ''
    });

    this.borrowForm = this.fb.group({
      lenderId: null,
      borrowerId: null,
      amount: null
    });

    this.expenseForm = this.fb.group({
      userId: null,
      memberIds: [],
      amount: null
    })

    this.earningForm = this.fb.group({
      userId: null,
      memberIds: [],
      amount: null
    })
  }

  getUsers() {
    this.appService.getUsers().subscribe((res: Array<any>) => {
      this.users = res;
    });
  }

  borrow() {
    this.appService.borrow(this.borrowForm.value).subscribe(res => {
      this.getUsers();
      this.currentModal.close();
      this.borrowError = '';
    }, err => {
      this.borrowError = err.error;
    });
  }

  addUser() {
    this.appService.addUser(this.userForm.value).subscribe(res => {
      this.getUsers();
      this.currentModal.close();
      this.error = '';
    }, err => {
      this.error = err.error
    })
    
  }

  addExpense() {
    const value = this.expenseForm.value;
    const expenseModel = {
      userId: value.userId,
      amount: value.amount,
      memberIds: value.memberIds ? value.memberIds.map(m => m.userId) : []
    }

    this.appService.addExpense(expenseModel).subscribe(res => {
      this.getUsers();
      this.currentModal.close();
      this.expenseError = '';
    }, err => {
      this.expenseError = err.error
    })
  }

  
  addEarning() {
    const value = this.earningForm.value;
    const earningModel = {
      userId: value.userId,
      amount: value.amount,
      memberIds: value.memberIds ? value.memberIds.map(m => m.userId) : []
    }

    this.appService.addEarning(earningModel).subscribe(res => {
      this.getUsers();
      this.currentModal.close();
      this.earningError = '';
    }, err => {
      this.earningError = err.error
    })
  }

  openModal(modal) {
    this.currentModal = this.modalService.open(modal);
    this.currentModal.result.then((result) => {
    }, (reason) => {
    })
  }

  getDetails(user, modal) {
    this.appService.getDebtsForUser(user.userId).subscribe((res: Array<any>) => {
      this.pickedDebts = res.filter(d => d.amount !== 0);
      if (this.pickedDebts.length === 0) {
        this.pickedDebts = null;
      }
      this.openModal(modal);
    })
  }

  deleteUser(user) {
    this.appService.deleteUser(user.userId).subscribe(res => {
      this.getUsers();

      this.expenseForm = this.fb.group({
        userId: null,
        memberIds: null,
        amount: null
      })

      this.earningForm = this.fb.group({
        userId: null,
        memberIds: null,
        amount: null
      })
    }, err => {
      console.log(err)
    })
  }

  colorOfState(state: number) {
    return {
      'color': state > 0 ? 'red' : 'green'
    }
  }

  rowColor(amount: number) {
    return {
      'background-color': amount > 0 ? 'lightcoral' : 'lightgreen'
    }
  }
}
