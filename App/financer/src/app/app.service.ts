import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get('http://localhost:8080/api/user');
  }

  addUser(user: any) {
    return this.http.post('http://localhost:8080/api/user', user);
  }

  deleteUser(userId: number) {
    return this.http.delete(`http://localhost:8080/api/user/${userId}`);
  }

  getDebtsForUser(userId: number) {
    return this.http.get(`http://localhost:8080/api/debt/${userId}`);
  }

  borrow(model) {
    return this.http.post("http://localhost:8080/api/debt/borrow", model);
  }

  addExpense(model) {
    return this.http.post("http://localhost:8080/api/debt/expense", model);
  }

  addEarning(model) {
    return this.http.post("http://localhost:8080/api/debt/earning", model);
  }
}
